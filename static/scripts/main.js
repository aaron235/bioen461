// load in the data
window.onload = function() {
	var timeElapsedData;
	d3.csv( "/static/data.csv", function( data ) {
		// parse everything as a number
		timeElapsedDataCols = data.map( function( d ) {
			return [ +d.time, +d.emg, +d.hr ];
		} );

		// grab the first timestamp
		start = timeElapsedDataCols[0][0];
		// properly format the time data
		timeElapsedDataCols.forEach( function( entry, i, arr ) {
			//subtract off the start
			entry[0] -= start;
			// normalize the EMG to roughly the right visual range
			entry[1] = entry[1] / 5;
			// milliseconds to seconds conversion with some rounding
			entry[0] = +( entry[0] / 1000 ).toFixed( 3 );

		} );

		timeElapsedData = timeElapsedDataCols.transpose();

		console.log( timeElapsedData );

		// console.log( timeElapsedData );

		var chart = c3.generate( {
			data: {
				x: 'Elapsed Time',
				columns: [
					new Array( "Elapsed Time" ).concat( timeElapsedData[0] ),
					new Array( "EMG" ).concat( timeElapsedData[1] ),
					new Array( "Heart Rate" ).concat( timeElapsedData[2] )
				]
			},
			regions: [
				{axis: 'x', end: .3, class: 'calm'},
				{axis: 'x', start: 151, end: 300, class: 'heights'},
				{axis: 'x', start: 300, class: 'claustrophobia'},
			]
		} );
	});

};
