import serial
from time import time
import os
import webbrowser

# port that the Teensy lives on (this is the default in linux)
serialPort = "/dev/ttyACM0"
# where to put the log file
logFile = "static/data.csv"


if __name__ == '__main__':
	# start up a serial connection
	try:
		teensy = serial.Serial( serialPort, baudrate=115200, timeout=1 )
		print( "Teensy initialized on port '" + teensy.port + "'" )
	except:
		raise ValueError( "Teensy not found on port '" + serialPort + "'" )

	# if it exists, open it, if not, make it, give it the right header, then open it
	# if os.path.isfile( logFile ):
	#	f = open( logFile, "w" )
	# else:
	f = open( logFile, "w" )
	f.write( "time,emg,hr\n" )

	try:
		# tell the teensy to start
		teensy.write( b's' )
		# note the time
		startTime = time() * 1000
		# clear out the first fragment of a line
		while teensy.read() != b'\n':
			pass

		# read until you get a kill signal
		while True:
			# read a line and format it as csv with a timestamp
			line = teensy.readline()
			lineStr = line.decode( 'utf-8' ).strip()
			timeLineStr = str( time() * 1000 - startTime ) + ',' + lineStr

			# nice string formatting
			data = timeLineStr.split( ',' )
			taggedData = zip( ["Time", "EMG", "HR"], data )
			dataLines = [x[0] + ": " + str( x[1] ) for x in taggedData]

			dataBlockPrint = ' '.join( dataLines )

			print( dataBlockPrint, end='\r', flush=True )

			f.write( timeLineStr + '\n' )
	except KeyboardInterrupt:
		# tell the teensy to stop
		teensy.write( b'x' )
		teensy.close()

		# clean the console
		print()

		# close up the file
		f.close()

		webbrowser.open( 'localhost:5000' )

		pass
