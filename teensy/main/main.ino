#include <elapsedMillis.h>

#define EMG A0
#define HR A1

#define START 's'
#define STOP 'x'

// Volatile Variables, used in service routine!
int thisTime;
int lastTime;
volatile int BPM;   // Beats per minute
volatile int BPMb; //Beats per bit
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded!
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat".
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.


// will probably not work faster than 5000 Hz
// default of 100 is good for the intended data
#define RATE 100 // Hz
const int DELAY = (1000000/RATE); // us

elapsedMicros timer;

// Start up a serial to plot to
void setup() {
	Serial.begin( 115200 );

	timer = 0;

  lastTime = micros();

}

// on boot, don't do anything
boolean run = false;

void loop() {
	if ( timer > DELAY ) {    
    // reset the timer
		timer -= DELAY;
		// Start when you get 's', stop when you get 'x'
		if (Serial.available() > 0) {
			char ctrl = Serial.read();
			if( ctrl == START ) {
				run = true;
			} else if ( ctrl == STOP ) {
				run = false;
			}
		}

   run = true;

   thisTime = timer;

		// If you're running, send csv over the serial as fast as you can print
		if( run ) {     
      getPulse();
			float emgVal = analogRead( EMG );
			float hrVal = analogRead( HR );
			String outStr = String( String( emgVal ) ); //+ ',' + String( hrVal ) + "," + String( BPM ) );

			Serial.println( outStr );
		}
	}
}
