# basic webapp functionality
from flask import Flask
from flask import render_template
from flask import request

# for reading in the stored biodata
import csv

# load up dat CSV
with open( 'static/data.csv', 'r' ) as f:
	reader = csv.reader( f )
	bioData = list(reader)

# make our app, in debug mode
app = Flask( __name__ )
app.debug = True
# make templating work nicer
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True


# '/main' is just a page with a graph
@app.route( '/' )
def main():
	return render_template( "data.html" )


# C3 will ask for this, this just returns the CSV from the times asked for
@app.route( '/data' )
def getData():
	if ( not set( ['start', 'length'] ) <= set( request.args ) ):
		return( "Error: Missing parameters for starting point and/or length", 400 )
	# tStart = request.args['start']
	# length = request.args['length']

	return '\n'.join( [','.join( s ) for s in bioData] )


app.run()
