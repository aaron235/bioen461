# Panic Lens

![logo](http://imgur.com/ioonbtL.png)

Welcome to Panic Lens! Panic Lens is a comprehensive software-and-hardware solution for metering user engagement in VR using biosignals. This repository contains this documentation and code for both the target computer and the Teensy 3.2 microcontroller.

![Mounted on the headset](http://imgur.com/DoHy651.jpg)

## Hardware

The Panic Lens is built on a Teensy 3.2 (https://www.pjrc.com/store/teensy32.html). The Teensy serves as the ADC and signal processor for the heart beat counter (https://www.sparkfun.com/products/11574) and the ADC for the MyoWare EMG sensor (https://www.sparkfun.com/products/13723). The trio is connected to the HTC Vive through the Teensy's USB port, into the Vive's extra USB port. (Important note: The Vive headset only accepts certain USB cables; the most effective are cables aith a 90 degree angled connector on the USB Type A Male connector side). All of this is connected and housed in a 3D printed enclosure for convenience.

![Panic Lens in housing](http://imgur.com/TZbPDTW.jpg)

### Setup

1. Ensure the Teensy has the correct code flashed before setting it up, as it is hard to do this step when the Teensy is already head-mounted.
2. Insert the heart rate monitor's red wire into the Teensy's 3.3V pin, the black wire into AGND, and the purple wire into A1. Connect the MyoWare Muscle Sensor to the Teensy by connecting "+" to Vin, "-" to GND, and "SIG" to A0. (For a reference on the pin names and locations for the Teensy 3.2, see https://www.pjrc.com/teensy/pinout.html).
3. Mount the Teensy on the Vive headset. An easy to do this is to 3D print the housing included in the design files folder in this repository, but any mounting system that keeps the sensors attached and the microcontroller is good.

### Usage

1. Attach adhesive snap electrodes to the MyoWare EMG sensor.
2. Affix the electrodes to the shoulder with the ground electrode roughly beneath the ear and the other two electrodes in line with it on the lower neck, with the wire connecton with the ground electrode pointing towards the ground electrode.
3. Clip the heart rate sensor on to the ear.

That's it! Your hardware is ready to collect data.

## Software

There are a few initial setup steps necessary before running Panic Lens.

### Setup

1. Install Teensyduino. See here: https://www.pjrc.com/teensy/teensyduino.html
2. Clone this repository and open the `/teensy/main/` folder. From here, flash `main.ino` onto the Teensy.
3. Set up the Python environment as follows:
    a. Ensure you have Python and virtualenv installed. See https://www.python.org/
    b. Set up a Python 3 virtual environment in the root folder of this repository:
    ```
    $ virtualenv --python=/usr/bin/python3 venv
    ```
    c. Activate the virtual environment:
    ```
    $ . venv/bin/activate
    ```
    d. Install the requirements:
    ```
    $ pip install -r requirements.txt
    ```
    
That's it for the software setup. 

### Usage

1. Activate the virtual environment (if you haven't already) and start up the webserver in the background:
   ```
   $ . venv/bin/activate
   $ python app.py &
   ```
2. Run the data collection (and start your game at the same time!)
   ```
   $ python serialRead.py
   ```
   Note: This will throw an error if the serial port is configured wrong or if the Teensy is not properly connected. Edit line 7 of `serialRead.py` so that `serialPort` is the value of your serial port. On UNIX based systems, this will look something like `/dev/ttyACM[X]`. To find your serial port, run `ls /dev`, plug in the Teensy, run `ls /dev` again, and the port that was just added will be the Teensy's serial port name.
   
   
That's all! When you close `serialRead.py` with a `^C` signal, it will automatically raise a web browser with aplot of your data.

### Troubleshooting

* If the Python program returns an error about access on the serial port, you may need to add yourself to the 'dialout' group. Do this as follows:
  ```
  $ sudo usermod -a -G dialout $USER
  ```
  You may need to log out and log back in again for these changes to take effect.






















